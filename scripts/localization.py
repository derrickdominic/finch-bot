
import rospy
import yaml
from finch_bot.msg import finch_feedback
from finch_bot.msg import finch_color

class localization(object):

    def mfrc_cb(self, mfrc):



        self.last_rfid = mfrc.uid;

    def sense_cb(self, fb):
        pass

    def color_cb(self, msg):
        pass

    def __init__(self, *args, **kwds):

        with open("../launch/map.yaml", 'r') as stream:
            self.dataMap = yaml.safe_load(stream);
            print(self.dataMap)

        self.last_rfid = -1;

        rospy.init_node('localization', anonymous=True)
        rospy.Subscriber('finch_mfrc',     finch_mfrc522,  self.mfrc_cb)
        rospy.Subscriber('finch_feedback', finch_feedback, self.sense_cb)
        rospy.Subscriber('finch_color',    finch_color,    self.color_cb)

        rospy.spin()

    def __del__(self):
        self.finch.close();

if __name__ == '__main__':
    lo = localization();
