#!/usr/bin/env python
import rospy
#from finch_bot.msg import finch_motor_cmd
from finch_bot.msg import finch_aux_cmd
from finch_bot.msg import finch_feedback
from finch_bot.msg import finch_color
from finch_bot.msg import finch_mfrc522
from finch_bot.msg import finch_follow_cmd
#from time import sleep
from blockext import *

# defaults
DEFAULTS={
    "direction": "forward",
    "duration": 30, # execute any dead reckon command for 30 secs
    "vel": 0.4,
    "units": 1,
    "lf_mode": "start",
    "sensor_side": "left",
    "channel": "red"
}

RED   = 0;
GREEN = 1;
BLUE  = 2;
CLEAR = 3;

COLOR_NAMES = ["RED", "GREEN", "BLUE", "CLEAR"];

class snap_server:

    def __init__(self, *args, **kwds):
        rospy.init_node('snap_server', anonymous=True)
        #rospy.Subscriber('finch_feedback', finch_feedback, self.feedback_cb)
        #self.pub = rospy.Publisher('finch_motor_cmd', finch_motor_cmd, queue_size=10)
        self.pub_aux = rospy.Publisher('finch_aux_cmd', finch_aux_cmd, queue_size=10)
        self.pub_follow = rospy.Publisher('finch_follow', finch_follow_cmd, queue_size=10)
        rospy.Subscriber('finch_color', finch_color, self.color_cb)
        rospy.Subscriber('finch_mfrc', finch_mfrc522, self.mfrc_cb)

        # init state
        self.left_colors  = [-1,-1,-1,-1] # -1 is error
        self.right_colors = [-1,-1,-1,-1] # -1 is error
        self.mfrc = {
            "utime": 0,
            "uid": -1,
            "long_id": "none"
        }

        # DO NOT ADD rospy.spin()

    def color_cb(self, msg):
        if (msg.id == 1):
            self.left_colors[RED]   = float(msg.rgbc[RED])
            self.left_colors[GREEN] = float(msg.rgbc[GREEN])
            self.left_colors[BLUE]  = float(msg.rgbc[BLUE])
            self.left_colors[CLEAR]  = float(msg.rgbc[CLEAR])
        else:
            self.right_colors[RED]   = float(msg.rgbc[RED])
            self.right_colors[GREEN] = float(msg.rgbc[GREEN])
            self.right_colors[BLUE]  = float(msg.rgbc[BLUE])
            self.right_colors[CLEAR]  = float(msg.rgbc[CLEAR])

    def mfrc_cb(self, msg):
        self.mfrc["utime"] = msg.utime
        self.mfrc["uid"] = msg.uid
        self.mfrc["long_id"] = msg.long_id

    @staticmethod
    def clamp(x, minimum, maximum):
        return min(max(x, minimum), maximum)

    @staticmethod
    def clamp_vel(vel):  # can range from -1 to 1 (0 to stop)
        return snap_server.clamp(vel, -1.0, 1.0)

    @staticmethod
    def clamp_color(c):  # can range from 0 to 255 (integers)
        return int(snap_server.clamp(c, 0, 255))

    @command("set drive %m.direction", is_blocking=True, \
             defaults=[DEFAULTS["direction"]])
    def set_drive(self, direction):
        self.set_drive_vel(direction, DEFAULTS["vel"])

    @command("set drive stop", is_blocking=True)
    def set_drive_stop(self):
        self.set_drive_vel("forward", 0)

    @command("set drive %m.direction at velocity %n", is_blocking=True, \
             defaults=[DEFAULTS["direction"], DEFAULTS["vel"]])
    def set_drive_vel(self, direction, vel):
        print "set drive %s at velocity %.2f" % (direction, vel)
        cvel = self.clamp_vel(vel)
        fc = finch_follow_cmd()
        fc.mode = fc.MODE_DEAD
        fc.time = DEFAULTS["duration"]
        if (direction == "forward"):
            fc.left_mtr  = cvel
            fc.right_mtr = cvel
        elif (direction == "left"):
            fc.left_mtr  = 0
            fc.right_mtr = cvel
        elif (direction == "right"):
            fc.left_mtr  = cvel
            fc.right_mtr = 0
        elif (direction == "left in place"):
            fc.left_mtr  = -cvel
            fc.right_mtr = cvel
        elif (direction == "right in place"):
            fc.left_mtr  = cvel
            fc.right_mtr = -cvel
        else:
            print "Invalid direction: commanding drive stop"
            fc.left_mtr  = 0
            fc.right_mtr = 0
        self.pub_follow.publish(fc)

    @command("set drive left at %n and right at %n", is_blocking=True, \
             defaults=[DEFAULTS["vel"], DEFAULTS["vel"]])
    def set_drive_custom(self, lvel, rvel):
        print "set drive left at %.2f and right at %.2f" % (lvel, rvel)
        fc = finch_follow_cmd()
        fc.mode = fc.MODE_DEAD
        fc.time = DEFAULTS["duration"]
        fc.left_mtr = self.clamp_vel(lvel)
        fc.right_mtr = self.clamp_vel(rvel)
        self.pub_follow.publish(fc)

    @command("go forward %n units", is_blocking=True, \
             defaults=[DEFAULTS["units"]])
    def go_forward(self, goal_units):

        if (goal_units < 0):
            return
        goal = int(goal_units)

        # set a timeout of 10 seconds
        timeout = rospy.Time.now() + rospy.Duration(10)

        curr_units = 0
        on_red = 0

        self.set_led_color_raw(0,0,0)
        while (rospy.Time.now() < timeout and curr_units < goal):
            self.set_line_follow("start")
            if (on_red == 0 and (self.get_line_color("left") == "red" or \
                                 self.get_line_color("right") == "red")):
                on_red = 1
                self.set_led_color_raw(255,0,0)
            if (on_red == 1 and not (self.get_line_color("left") == "red" or \
                                     self.get_line_color("right") == "red")):
                on_red = 0
                curr_units = curr_units + 1
                # update timeout for next unit
                timeout = rospy.Time.now() + rospy.Duration(10)
                self.set_led_color_raw(0,0,0)
            rospy.sleep(0.05)
        self.set_line_follow("stop")

    @command("set line follow %m.lf_mode", is_blocking=True, \
             defaults=[DEFAULTS["lf_mode"]])
    def set_line_follow(self, lf_mode):
        print "set line follow %s" % (lf_mode)
        fc = finch_follow_cmd()
        if (lf_mode == "start"):
            fc.mode = fc.MODE_LINE
        else:
            if (lf_mode != "stop"):
                print "Invalid mode: commanding line follow stop"
            fc.mode = fc.MODE_STOP
        self.pub_follow.publish(fc)

    @command("set led to %c", is_blocking=True)
    def set_led_color(self, c):
        print "set led to (%d, %d, %d)" % (c[0], c[1], c[2])
        ac = finch_aux_cmd()
        ac.led = c
        self.pub_aux.publish(ac)

    @command("set led to %n %n %n", is_blocking=True)
    def set_led_color_raw(self, r, g, b):
        print "set led to (%d, %d, %d)" % (r, g, b)
        ac = finch_aux_cmd()
        ac.led = [self.clamp_color(r), self.clamp_color(g), self.clamp_color(b)]
        self.pub_aux.publish(ac)

    # returns "red", "black", or "white"
    # per side, only between those 3 colors
    @reporter("get line color on %m.sensor_side", is_blocking=True, \
             defaults=[DEFAULTS["sensor_side"]])
    def get_line_color(self, sensor_side):
        print "get line color on %s" % (sensor_side)
        if (sensor_side == "left"):
            colors = self.left_colors
        elif (sensor_side == "right"):
            colors = self.right_colors
        else:
            print "Invalid sensor side: returning error"
            return "error"

        if (colors[RED] / colors[CLEAR] > 0.5):
            return "red"
        elif (colors[CLEAR] > 100):
            return "white"
        else:
            return "black"

    @reporter("get raw color on %m.sensor_side channel %m.channel", \
              is_blocking=True, defaults=[DEFAULTS["sensor_side"], \
                                          DEFAULTS["channel"]])
    def get_raw_color(self, sensor_side, channel):
        print "get raw color on %s channel %s" % (sensor_side, channel)
        if (sensor_side == "left"):
            colors = self.left_colors
        elif (sensor_side == "right"):
            colors = self.right_colors
        else:
            print "Invalid sensor side: returning -1"
            return -1

        if (channel == "red"):
            return colors[RED]
        elif (channel == "green"):
            return colors[GREEN]
        elif (channel == "blue"):
            return colors[BLUE]
        elif (channel == "clear"):
            return colors[CLEAR]
        else:
            print "Invalid color channel, returning -1"
            return -1

    @reporter("get rfid tag uid")
    def get_rfid_tag_uid(self):
        print "get rfid tag uid"
        return self.mfrc["uid"]

    @reporter("get rfid tag long_id")
    def get_rfid_tag_long_id(self):
        print "get rfid tag long_id"
        return self.mfrc["long_id"]

    @reporter("get traffic light state")
    def get_traffic_light(self): # TODO
        return "error"

descriptor = Descriptor(
    name = "Finch Bot",
    port = 1234,
    blocks = get_decorated_blocks_from_class(snap_server),
    menus = dict(
        direction = ["forward", "left", "right", "left in place", "right in place"],
        lf_mode = ["start", "stop"],
        sensor_side = ["left", "right"],
        channel = ["red", "green", "blue", "clear"]
    )
)

extension = Extension(snap_server, descriptor)

if __name__ == "__main__":
    extension.run_forever(debug=True)
