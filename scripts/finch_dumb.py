
import rospy
from finch_bot.msg import finch_motor_cmd
from finch_bot.msg import finch_aux_cmd
from finch_bot.msg import finch_feedback

class finch_dumb(object):

    def sense_cb(self, fb):
        mc = finch_motor_cmd();
        ac = finch_aux_cmd();

        if fb.tap and not self.pause_hist:
            self.pause =  not self.pause;

        self.pause_hist = fb.tap

        mc.left = 0.0;
        mc.right = 0.0;
        ac.led = [50, 50, 50];

        if not self.pause :
            ac.led = [255, 0, 0];
            thresh = 0.25

            if fb.left_light > thresh:
                mc.left = 0.0;
                mc.right = 0.5;
                ac.led = [255, 255, 0];

            if fb.right_light > thresh:
                mc.left = 0.5;
                mc.right = 0;
                ac.led = [0, 255, 255];

            if fb.right_light > thresh and fb.left_light > thresh :
                mc.left = 0.5;
                mc.right = 0.5;
                ac.led = [ 0, 255, 0];

        self.pub.publish(mc)
        self.pub_aux.publish(ac)

    def __init__(self, *args, **kwds):

        self.pause = True;
        self.pause_hist = False;

        rospy.init_node('finch_dumb', anonymous=True)

        self.pub = rospy.Publisher('finch_motor_cmd', finch_motor_cmd, queue_size=10)
        self.pub_aux = rospy.Publisher('finch_aux_cmd', finch_aux_cmd, queue_size=10)

        rospy.Subscriber('finch_feedback', finch_feedback, self.sense_cb)

        rospy.spin()

    def __del__(self):
        self.finch.close();

if __name__ == '__main__':
    fd = finch_dumb();
